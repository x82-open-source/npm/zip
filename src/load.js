
import { isNode, isStream } from './nodejsUtils';
import { prepareContent } from './utils';
import Crc32Probe from './stream/Crc32Probe';
import ZipEntries from './zipEntries';
import kewkew from 'kewkew';
import utf8 from './utf8';
/**
 * Check the CRC32 of an entry.
 * @param {ZipEntry} zipEntry the zip entry to check.
 * @return {Promise} the result.
 */
function checkEntryCRC32(zipEntry) {
    var promise = kewkew.defer();
    var worker = zipEntry.decompressed.getContentWorker()
        .pipe(new Crc32Probe());
    worker.on('error', function (e) {
        promise.reject(e);
    })
        .on('end', function () {
            if (worker.streamInfo.crc32 !== zipEntry.decompressed.crc32) {
                promise.reject(new Error('Corrupted zip : CRC32 mismatch'));
            } else {
                promise.resolve();
            }
        })
        .resume();
    return promise;
}
export default function (data, options) {
    var self = this;
    options = {
        base64: false,
        checkCRC32: false,
        optimizedBinaryString: false,
        createFolders: false,
        decodeFileName: utf8.utf8decode,
        ...options
    };
    if (isNode && isStream(data)) {
        return kewkew.reject(new Error('JSZip can\'t accept a stream when loading a zip file.'));
    }
    return prepareContent('the loaded zip file', data, true, options.optimizedBinaryString, options.base64)
        .then(function (data) {
            var zipEntries = new ZipEntries(options);
            zipEntries.load(data);
            return zipEntries;
        })
        .then(function checkCRC32(zipEntries) {
            var promises = [kewkew.resolve(zipEntries)];
            var files = zipEntries.files;
            if (options.checkCRC32) {
                for (var i = 0; i < files.length; i++) {
                    promises.push(checkEntryCRC32(files[i]));
                }
            }
            return kewkew.all(promises);
        })
        .then(function addFiles(results) {
            var zipEntries = results.shift();
            var files = zipEntries.files;
            for (var i = 0; i < files.length; i++) {
                var input = files[i];
                self.file(input.fileNameStr, input.decompressed,
                    {
                        binary: true,
                        optimizedBinaryString: true,
                        date: input.date,
                        dir: input.dir,
                        comment: input.fileCommentStr.length ? input.fileCommentStr : null,
                        unixPermissions: input.unixPermissions,
                        dosPermissions: input.dosPermissions,
                        createFolders: options.createFolders
                    });
            }
            if (zipEntries.zipComment.length) {
                self.comment = zipEntries.zipComment;
            }
            return self;
        });
};