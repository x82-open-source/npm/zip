
import { inherits, transformTo } from './utils';
import GenericWorker from './stream/GenericWorker';
import pako from 'pako';
const USE_TYPEDARRAY = (typeof Uint8Array !== 'undefined') && (typeof Uint16Array !== 'undefined') && (typeof Uint32Array !== 'undefined');

const ARRAY_TYPE = USE_TYPEDARRAY ? 'uint8array' : 'array';
export const magic = '\x08\x00';
/**
 * Create a worker that uses pako to inflate/deflate.
 * @constructor
 * @param {String} action the name of the pako function to call : either 'Deflate' or 'Inflate'.
 * @param {Object} options the options to use when (de)compressing.
 */
function FlateWorker(action, options) {
    GenericWorker.call(this, 'FlateWorker/' + action);
    this._pako = new pako[action](
        {
            raw: true,
            level: options.level || -1 // default compression
        });
    // the `meta` object from the last chunk received
    // this allow this worker to pass around metadata
    this.meta = {};
    var self = this;
    this._pako.onData = function (data) {
        self.push(
            {
                data: data,
                meta: self.meta
            });
    };
}
inherits(FlateWorker, GenericWorker);
FlateWorker.prototype.processChunk = function (chunk) {
    this.meta = chunk.meta;
    this._pako.push(transformTo(ARRAY_TYPE, chunk.data), false);
};
/**
 * @see GenericWorker.flush
 */
FlateWorker.prototype.flush = function () {
    GenericWorker.prototype.flush.call(this);
    this._pako.push([], true);
};
/**
 * @see GenericWorker.cleanUp
 */
FlateWorker.prototype.cleanUp = function () {
    GenericWorker.prototype.cleanUp.call(this);
    this._pako = null;
};

export const compressWorker = function (compressionOptions) {
    return new FlateWorker('Deflate', compressionOptions);
};
export const uncompressWorker = function () {
    return new FlateWorker('Inflate',
        {});
};