import { inherits, transformTo } from '../utils';
import GenericWorker from './GenericWorker';
/**
 * A worker which convert chunks to a specified type.
 * @constructor
 * @param {String} destType the destination type.
 */
function ConvertWorker(destType) {
	GenericWorker.call(this, 'ConvertWorker to ' + destType);
	this.destType = destType;
}
inherits(ConvertWorker, GenericWorker);
/**
 * @see GenericWorker.processChunk
 */
ConvertWorker.prototype.processChunk = function (chunk) {
	this.push(
		{
			data: transformTo(this.destType, chunk.data),
			meta: chunk.meta
		});
};
export default ConvertWorker;