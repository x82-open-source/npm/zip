
/**
  * True if this is running in Nodejs, will be undefined in a browser.
  * In a browser, browserify won't include this file and the whole module
  * will be resolved an empty object.
  */
export const isNode = typeof Buffer !== 'undefined';
/**
 * Create a new nodejs Buffer.
 * @param {string|number} data the data to pass to the constructor.
 * @param {String} encoding the encoding to use.
 * @return {Buffer} a new Buffer.
 */
export const newBuffer = (data, encoding) => {
  if (typeof data === 'number') {
    return Buffer.alloc(data, encoding);
  }
  return Buffer.from(data, encoding);
};
/**
   * Find out if an object is a Buffer.
   * @param {Object} b the object to test.
   * @return {Boolean} true if the object is a Buffer, false otherwise.
   */
export const isBuffer = (b) => Buffer.isBuffer(b);
export const isStream = (obj) => obj && typeof obj.on === 'function' && typeof obj.pause === 'function' && typeof obj.resume === 'function';