export default {
    base64: false,
    array: true,
    string: true,
    arraybuffer: typeof ArrayBuffer !== 'undefined' && typeof Uint8Array !== 'undefined',
    // contains true if JSZip can read/generate Uint8Array, false otherwise.
    nodebuffer: typeof Buffer !== 'undefined',
    uint8array: typeof Uint8Array !== 'undefined',
    nodestream: true,
    blob: false
};