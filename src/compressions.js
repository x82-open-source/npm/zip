import { compressWorker, magic, uncompressWorker } from './flate';
import GenericWorker from './stream/GenericWorker';

export default {
	STORE: {
		magic: '\x00\x00',
		compressWorker: function () {
			return new GenericWorker('STORE compression');
		},
		uncompressWorker: function () {
			return new GenericWorker('STORE decompression');
		}
	},
	DEFLATE: {
		magic,
		compressWorker,
		uncompressWorker
	}
};