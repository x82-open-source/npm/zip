
import { applyFromCharCode, inherits, transformTo } from './utils';
import { newBuffer } from './nodejsUtils';
import GenericWorker from './stream/GenericWorker';
import support from './support';
/**
 * The following functions come from pako, from pako/lib/utils/strings
 * released under the MIT license, see pako https://github.com/nodeca/pako/
 */
// Table with utf8 lengths (calculated by first byte of sequence)
// Note, that 5 & 6-byte values and some 4-byte values can not be represented in JS,
// because max possible codepoint is 0x10ffff
var _utf8len = new Array(256);
for (var i = 0, toAdd; i < 256; i++) {
    if (i >= 252) {
        toAdd = 6;
    } else if (i >= 248) {
        toAdd = 5;
    } else if (i >= 240) {
        toAdd = 4;
    } else if (i >= 224) {
        toAdd = 3;
    } else if (i >= 192) {
        toAdd = 2;
    } else {
        toAdd = 1;
    }
    _utf8len[i] = toAdd;
}
_utf8len[254] = _utf8len[254] = 1; // Invalid sequence start
// convert string to array (typed, when possible)
var string2buf = function (str) {
    var buf,
        c,
        c2,
        memoryPosition,
        i,
        stringLength = str.length,
        toAdd,
        bufferLength = 0;
    // count binary size
    for (memoryPosition = 0; memoryPosition < stringLength; memoryPosition++) {
        c = str.charCodeAt(memoryPosition);
        if ((c & 0xfc00) === 0xd800 && (memoryPosition + 1 < stringLength)) {
            c2 = str.charCodeAt(memoryPosition + 1);
            if ((c2 & 0xfc00) === 0xdc00) {
                c = 0x10000 + ((c - 0xd800) << 10) + (c2 - 0xdc00);
                memoryPosition++;
            }
        }
        if (c < 0x80) {
            toAdd = 1;
        } else if (c < 0x800) {
            toAdd = 2;
        } else if (c < 0x10000) {
            toAdd = 3;
        } else {
            toAdd = 4;
        }
        bufferLength += toAdd;
    }
    // allocate buffer
    if (support.uint8array) {
        buf = new Uint8Array(bufferLength);
    } else {
        buf = new Array(bufferLength);
    }
    // convert
    for (i = 0, memoryPosition = 0; i < bufferLength; memoryPosition++) {
        c = str.charCodeAt(memoryPosition);
        if ((c & 0xfc00) === 0xd800 && (memoryPosition + 1 < stringLength)) {
            c2 = str.charCodeAt(memoryPosition + 1);
            if ((c2 & 0xfc00) === 0xdc00) {
                c = 0x10000 + ((c - 0xd800) << 10) + (c2 - 0xdc00);
                memoryPosition++;
            }
        }
        if (c < 0x80) {
            /* one byte */
            buf[i++] = c;
        } else if (c < 0x800) {
            /* two bytes */
            buf[i++] = 0xC0 | (c >>> 6);
            buf[i++] = 0x80 | (c & 0x3f);
        } else if (c < 0x10000) {
            /* three bytes */
            buf[i++] = 0xE0 | (c >>> 12);
            buf[i++] = 0x80 | (c >>> 6 & 0x3f);
            buf[i++] = 0x80 | (c & 0x3f);
        } else {
            /* four bytes */
            buf[i++] = 0xf0 | (c >>> 18);
            buf[i++] = 0x80 | (c >>> 12 & 0x3f);
            buf[i++] = 0x80 | (c >>> 6 & 0x3f);
            buf[i++] = 0x80 | (c & 0x3f);
        }
    }
    return buf;
};
// Calculate max possible position in utf8 buffer,
// that will not break sequence. If that's not possible
// - (very small limits) return max size as is.
//
// buf[] - utf8 bytes array
// max   - length limit (mandatory);
var utf8border = function (buf, max) {
    var pos;
    max = max || buf.length;
    if (max > buf.length) {
        max = buf.length;
    }
    // go back from last position, until start of sequence found
    pos = max - 1;
    while (pos >= 0 && (buf[pos] & 0xC0) === 0x80) {
        pos--;
    }
    // Fuckup - very small and broken sequence,
    // return max, because we should return something anyway.
    if (pos < 0) {
        return max;
    }
    // If we came to start of buffer - that means vuffer is too small,
    // return max too.
    if (pos === 0) {
        return max;
    }
    return (pos + _utf8len[buf[pos]] > max) ? pos : max;
};
// convert array to string
var buf2string = function (buf) {
    var i, out, c, characterLength;
    var len = buf.length;
    // Reserve max possible length (2 words per char)
    // NB: by unknown reasons, Array is significantly faster for
    //     String.fromCharCode.apply than Uint16Array.
    var utf16buf = new Array(len * 2);
    for (out = 0, i = 0; i < len;) {
        c = buf[i++];
        // quick process ascii
        if (c < 0x80) {
            utf16buf[out++] = c;
            continue;
        }
        characterLength = _utf8len[c];
        // skip 5 & 6 byte codes
        if (characterLength > 4) {
            utf16buf[out++] = 0xfffd;
            i += characterLength - 1;
            continue;
        }
        // apply mask on first byte
        if (characterLength === 2) {
            c &= 0x1;
        } else if (characterLength === 3) {
            c &= 0x0f;
        } else {
            c &= 0x07;
        }
        // join the rest
        while (characterLength > 1 && i < len) {
            c = (c << 6) | (buf[i++] & 0x3f);
            characterLength--;
        }
        // terminated by end of string?
        if (characterLength > 1) {
            utf16buf[out++] = 0xfffd;
            continue;
        }
        if (c < 0x10000) {
            utf16buf[out++] = c;
        } else {
            c -= 0x10000;
            utf16buf[out++] = 0xd800 | ((c >> 10) & 0x3ff);
            utf16buf[out++] = 0xdc00 | (c & 0x3ff);
        }
    }
    // shrinkBuf(utf16buf, out)
    if (utf16buf.length !== out) {
        if (utf16buf.subarray) {
            utf16buf = utf16buf.subarray(0, out);
        } else {
            utf16buf.length = out;
        }
    }
    // return String.fromCharCode.apply(null, utf16buf);
    return applyFromCharCode(utf16buf);
};
// That's all for the pako functions.
/**
 * Transform a javascript string into an array (typed if possible) of bytes,
 * UTF-8 encoded.
 * @param {String} str the string to encode
 * @return {Array|Uint8Array|Buffer} the UTF-8 encoded string.
 */
exports.utf8encode = function utf8encode(str) {
    if (support.nodebuffer) {
        return newBuffer(str, 'utf-8');
    }
    return string2buf(str);
};
/**
 * Transform a bytes array (or a representation) representing an UTF-8 encoded
 * string into a javascript string.
 * @param {Array|Uint8Array|Buffer} buf the data de decode
 * @return {String} the decoded string.
 */
exports.utf8decode = function utf8decode(buf) {
    if (support.nodebuffer) {
        return transformTo('nodebuffer', buf)
            .toString('utf-8');
    }
    buf = transformTo(support.uint8array ? 'uint8array' : 'array', buf);
    return buf2string(buf);
};
/**
 * A worker to decode utf8 encoded binary chunks into string chunks.
 * @constructor
 */
export function Utf8DecodeWorker() {
    GenericWorker.call(this, 'utf-8 decode');
    // the last bytes if a chunk didn't end with a complete codepoint.
    this.leftOver = null;
}
inherits(Utf8DecodeWorker, GenericWorker);
Utf8DecodeWorker.prototype.processChunk = function (chunk) {
    var data = transformTo(support.uint8array ? 'uint8array' : 'array', chunk.data);
    // 1st step, re-use what's left of the previous chunk
    if (this.leftOver && this.leftOver.length) {
        if (support.uint8array) {
            var previousData = data;
            data = new Uint8Array(previousData.length + this.leftOver.length);
            data.set(this.leftOver, 0);
            data.set(previousData, this.leftOver.length);
        } else {
            data = this.leftOver.concat(data);
        }
        this.leftOver = null;
    }
    var nextBoundary = utf8border(data);
    var usableData = data;
    if (nextBoundary !== data.length) {
        if (support.uint8array) {
            usableData = data.subarray(0, nextBoundary);
            this.leftOver = data.subarray(nextBoundary, data.length);
        } else {
            usableData = data.slice(0, nextBoundary);
            this.leftOver = data.slice(nextBoundary, data.length);
        }
    }
    this.push(
        {
            data: exports.utf8decode(usableData),
            meta: chunk.meta
        });
};
/**
 * @see GenericWorker.flush
 */
Utf8DecodeWorker.prototype.flush = function () {
    if (this.leftOver && this.leftOver.length) {
        this.push(
            {
                data: exports.utf8decode(this.leftOver),
                meta:
                    {}
            });
        this.leftOver = null;
    }
};
/**
 * A worker to endcode string chunks into utf8 encoded binary chunks.
 * @constructor
 */
export function Utf8EncodeWorker() {
    GenericWorker.call(this, 'utf-8 encode');
}
inherits(Utf8EncodeWorker, GenericWorker);
Utf8EncodeWorker.prototype.processChunk = function (chunk) {
    this.push(
        {
            data: exports.utf8encode(chunk.data),
            meta: chunk.meta
        });
};
