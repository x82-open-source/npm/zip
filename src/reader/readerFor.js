import { checkSupport, getTypeOf, transformTo } from '../utils';
import ArrayReader from './ArrayReader';
import NodeBufferReader from './NodeBufferReader';
import StringReader from './StringReader';
import Uint8ArrayReader from './Uint8ArrayReader';
import support from '../support';
/**
 * Create a reader adapted to the data.
 * @param {String|ArrayBuffer|Uint8Array|Buffer} data the data to read.
 * @return {DataReader} the data reader.
 */
export default (data) => {
    const type = getTypeOf(data);
    checkSupport(type);
    if (type === 'string' && !support.uint8array) {
        return new StringReader(data);
    }
    if (type === 'nodebuffer') {
        return new NodeBufferReader(data);
    }
    if (support.uint8array) {
        return new Uint8ArrayReader(transformTo('uint8array', data));
    }
    return new ArrayReader(transformTo('array', data));
};