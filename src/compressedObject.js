import Crc32Probe from './stream/Crc32Probe';
import DataLengthProbe from './stream/DataLengthProbe';
import DataWorker from './stream/DataWorker';
import kewkew from 'kewkew';
/**
 * Represent a compressed object, with everything needed to decompress it.
 * @constructor
 * @param {number} compressedSize the size of the data compressed.
 * @param {number} uncompressedSize the size of the data after decompression.
 * @param {number} crc32 the crc32 of the decompressed file.
 * @param {object} compression the type of compression, see lib/compressions.js.
 * @param {String|ArrayBuffer|Uint8Array|Buffer} data the compressed data.
 */
function CompressedObject(compressedSize, uncompressedSize, crc32, compression, data) {
    this.compressedSize = compressedSize;
    this.uncompressedSize = uncompressedSize;
    this.crc32 = crc32;
    this.compression = compression;
    this.compressedContent = data;
}
CompressedObject.prototype = {
    /**
     * Create a worker to get the uncompressed content.
     * @return {GenericWorker} the worker.
     */
    getContentWorker: function () {
        var worker = new DataWorker(kewkew.resolve(this.compressedContent))
            .pipe(this.compression.uncompressWorker())
            .pipe(new DataLengthProbe('data_length'));
        var self = this;
        worker.on('end', function () {
            // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
            if (this.streamInfo.data_length !== self.uncompressedSize) {
                throw new Error('Bug : uncompressed data size mismatch');
            }
        });
        return worker;
    },
    /**
     * Create a worker to get the compressed content.
     * @return {GenericWorker} the worker.
     */
    getCompressedWorker: function () {
        return new DataWorker(kewkew.resolve(this.compressedContent))
            .withStreamInfo('compressedSize', this.compressedSize)
            .withStreamInfo('uncompressedSize', this.uncompressedSize)
            .withStreamInfo('crc32', this.crc32)
            .withStreamInfo('compression', this.compression);
    }
};
/**
 * Chain the given worker with other workers to compress the content with the
 * given compresion.
 * @param {GenericWorker} uncompressedWorker the worker to pipe.
 * @param {Object} compression the compression object.
 * @param {Object} compressionOptions the options to use when compressing.
 * @return {GenericWorker} the new worker compressing the content.
 */
CompressedObject.createWorkerFrom = function (uncompressedWorker, compression, compressionOptions) {
    return uncompressedWorker.pipe(new Crc32Probe())
        .pipe(new DataLengthProbe('uncompressedSize'))
        .pipe(compression.compressWorker(compressionOptions))
        .pipe(new DataLengthProbe('compressedSize'))
        .withStreamInfo('compression', compression);
};
export default CompressedObject;